﻿#include <iostream>

class Vector
{
    private:
        double x;
        double y;
        double z;
    public:
        Vector(): x(0), y(0), z(0)
        {
        }

        Vector(double _x, double _y, double _z)
        {
            x = _x;
            y = _y;
            z = _z;
        }
        void PrintValues()
        {
            std::cout << "X: " << x << "\nY: " << y << "\nZ: " << z << '\n' << '\n';
        }

        double VectorLength()
        {
            return sqrt(x*x + y*y + z*z);
        }
};

int main()
{
    Vector v1;
    v1.PrintValues();

    Vector v2(-1, 2, 0);
    v2.PrintValues();

    std::cout <<"Vector length: "<< v2.VectorLength() << '\n';
}

